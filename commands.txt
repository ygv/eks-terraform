sudo yum update -y && sudo yum install wget unzip -y
VERSION=0.12.25
sudo wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip -P /tmp
sudo unzip /tmp/terraform_${VERSION}_linux_amd64.zip -d /usr/local/bin/
terraform -v
sudo yum install git -y
git --version
git clone https://ygv@bitbucket.org/ygv/eks-terraform.git
sudo yum update -y
sudo yum install epel-release -y
sudo yum install centos-release-scl -y
sudo yum install rh-python36 -y
scl enable rh-python36 bash
python --version
sudo yum install python3-pip -y
pip3 --version
sudo yum install python3-devel -y
sudo yum groupinstall 'development tools' -y
pip3 install --upgrade --user awscli
aws --version
~/.local/bin/aws --version
alias aws='~/.local/bin/aws'
aws --version

#######################################################
sudo yum install awscli
###optional###sudo chmod -R 755 /usr/local/bin/aws
aws --version
########################################################
terraform init
terraform validate
terraform plan
terraform apply
##########################################################################################################
curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/kubectl
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc
kubectl version --short --client
kubectl version
curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.7/2019-03-27/bin/linux/amd64/aws-iam-authenticator
chmod +x ./aws-iam-authenticator
sudo mv ./aws-iam-authenticator /usr/local/bin/
aws sts get-caller-identity
aws eks --region us-east-1 describe-cluster --name eks_cluster_tuto --query cluster.status
aws eks --region us-east-1 update-kubeconfig --name eks_cluster_tuto
kubectl get nodes

#!/bin/bash 
cd /tmp 
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm 
sudo systemctl enable amazon-ssm-agent 
sudo systemctl start amazon-ssm-agent

aws ssm send-command --instance-ids "your id's" --document-name "AWS-RunShellScript" --comment "IP config" --parameters "commands=ifconfig" --output text

aws ssm send-command \
    --document-name "AWS-RunShellScript" \
    --parameters 'commands=["echo HelloWorld"]' \
    --targets "Key=instanceids,Values=i-1234567890abcdef0" \
    --comment "echo HelloWorld"

######################################################################################
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
sudo yum -y install packer
packer
aws s3 ls s3://amazon-eks/<version>
git clone https://github.com/aws-samples/amazon-eks-custom-amis.git
vi amazon-eks-node-centos7.json
vi Makefile
make build-centos7-1.21

https://github.com/awslabs/amazon-eks-ami/tree/master/files

eks dns ip = 10.100.0.10


aws ec2 describe-images --filters '[{"Name": "name", "Values": ["golden-ami-centos7*"]},{"Name": "virtualization-type", "Values": ["hvm"]},{"Name": "architecture", "Values": ["x86_64"]},{"Name": "image-type", "Values": ["machine"]}]' --query 'sort_by(Images, &CreationDate)[-1]' --region us-east-1 --output json