#!/bin/bash
set -ex
#sudo su - centos
#echo "EKS Cluster config update"
#sudo -u centos bash -c 'aws sts get-caller-identity'
#sudo -u centos bash -c 'aws eks --region "us-east-1" describe-cluster --name "dev-soar-cassandra-cluster" --query cluster.status'
#sudo -u centos bash -c 'aws eks --region "us-east-1" update-kubeconfig --name "dev-soar-cassandra-cluster"'
#sudo -u centos bash -c 'kubectl get nodes'
su centos <<EOUSER
#!/bin/bash
echo "EKS Cluster config update"
pwd
whoami
pip3 install --upgrade --user awscli
~/.local/bin/aws --version
echo $PATH
$ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
ll ~/.local/bin/aws
export PATH=$HOME/.local/bin:$PATH
alias aws='~/.local/bin/aws'
aws --version
aws sts get-caller-identity
aws eks --region "us-east-1" describe-cluster --name "dev-soar-cassandra-cluster" --query cluster.status

EOUSER
# Launch Template
#------------------------------------------------------------------------------
resource "aws_launch_template" "ec2_asg_launch_template" {
  name                    = "${var.env}-${var.app}-${var.microservice}-asg-launch-template"
  vpc_security_group_ids  = [var.cargill_managed_sg_id, var.common_dev_sg_id, var.cargill_default_sg_id]
  image_id                = var.ec2_ami
  instance_type           = var.ec2_instance_type
  iam_instance_profile {
    name                  = var.ec2_iam_instance_profile
  }
  key_name                = var.key_name
  user_data               = base64encode(local.ec2-userdata)
  tag_specifications {
    resource_type         = "instance"  
    tags                  = merge(local.tags, map("Name", "${var.env}-${var.app}-${var.microservice}-cluster-master"))
  }
  lifecycle {
    create_before_destroy = true
  }
}